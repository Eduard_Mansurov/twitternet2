﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TwitterWebFormGIT;

/// <summary>
/// Сводное описание для Twit
/// </summary>
public class Twit : DbContext
{
    public Twit()
    {
        
    }

    private DateTime date;
    private string location;
    private string twitText;
    private ApplicationUser user;
    private string access;

    public virtual DateTime Date
    {
        get
        {
            return date;
        }

        set
        {
            date = value;
        }
    }

    public virtual string Location
    {
        get
        {
            return location;
        }

        set
        {
            location = value;
        }
    }

    public virtual string TwitText
    {
        get
        {
            return twitText;
        }

        set
        {
            twitText = value;
        }
    }

    public virtual ApplicationUser User
    {
        get
        {
            return user;
        }

        set
        {
            user = value;
        }
    }

    public virtual string Access
    {
        get
        {
            return access;
        }

        set
        {
            access = value;
        }
    }
}