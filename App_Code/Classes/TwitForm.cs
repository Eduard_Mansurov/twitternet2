﻿using Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Сводное описание для Class1
/// </summary>
public class TwitForm
 
{

    private String userName;
    private string id;
    private string text;
    private DateTime date;

    public string UserName
    {
        get
        {
            return userName;
        }

        set
        {
            userName = value;
        }
    }

    public string Id
    {
        get
        {
            return id;
        }

        set
        {
            id = value;
        }
    }

    public string Text
    {
        get
        {
            return text;
        }

        set
        {
            text = value;
        }
    }

    public DateTime Date
    {
        get
        {
            return date;
        }

        set
        {
            date = value;
        }
    }

    public TwitForm(twit myTwit)
    {
        this.id = myTwit.Id;
        this.text = myTwit.twittext;
        this.UserName = myTwit.AspNetUsers.UserName;
        this.Date = myTwit.datetwit;

    }
}