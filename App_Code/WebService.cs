﻿using System;
using Classes;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

/// <summary>
/// Сводное описание для WebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// Чтобы разрешить вызывать веб-службу из скрипта с помощью ASP.NET AJAX, раскомментируйте следующую строку. 
// [System.Web.Script.Services.ScriptService]
public class WebService : System.Web.Services.WebService
{

    public WebService()
    {

        //Раскомментируйте следующую строку в случае использования сконструированных компонентов 
        //InitializeComponent(); 
    }
    
    [WebMethod]
    public String getTwits(string name)
    {
        List<twit> listTwits = new List<twit>();
        TwitRepo twitRepo = new TwitRepo();
        listTwits = twitRepo.GetByUserName(name);

        List<TwitForm> listBestTwit = new List<TwitForm>();
        foreach (twit elementTwit in listTwits)
        {
            listBestTwit.Add(new TwitForm(elementTwit));
        }
        

        var json = JsonConvert.SerializeObject(listBestTwit);

        return json;
    }

}
