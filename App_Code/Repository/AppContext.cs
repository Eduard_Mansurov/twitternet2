﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TwitterWebFormGIT;

/// <summary>
/// Сводное описание для AppContext
/// </summary>
public class AppContext : DbContext
{
    public AppContext() 
    {
        //
        // TODO: добавьте логику конструктора
        //
    }

    public DbSet<Twit> Twits { get; set; }
    
    public DbSet<ApplicationUser> Users { get; set; }
}