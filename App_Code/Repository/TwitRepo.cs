﻿using Classes;
using NHibernate;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

/// <summary>
/// Сводное описание для TwitRepo
/// </summary>
public class TwitRepo : AbstractRepo
{
    public TwitRepo()
    {
        
    }
    
    public void add(twit newTwit)
    {

        Entities.twit.Add(newTwit);
        try { 
            Entities.SaveChanges();
        }
        catch (DbEntityValidationException ex)
        {
            foreach (DbEntityValidationResult item in ex.EntityValidationErrors)
            {
                // Get entry

                DbEntityEntry entry = item.Entry;
                string entityTypeName = entry.Entity.GetType().Name;

                // Display or log error messages

                foreach (DbValidationError subItem in item.ValidationErrors)
                {
                    string message = string.Format("Error '{0}' occurred in {1} at {2}",
                             subItem.ErrorMessage, entityTypeName, subItem.PropertyName);
                    Console.WriteLine(message);
                }
            }
        }
    }

    public void Update(twit changedTwit)
    {
        Entities.twit.Attach(changedTwit);
        var entry = Entities.Entry(changedTwit);
        entry.State = EntityState.Modified;
        Entities.SaveChanges();
    }

    public void Delete(string twitId)
    {
        //twit someTwit = Entities.twit.SingleOrDefault(x => x.Id.Equals(twitId));
        twit someTwit = new twit();
        someTwit.Id = twitId;

        if (someTwit != null)
        {
            Entities.twit.Attach(someTwit);
            Entities.twit.Remove(someTwit);
            Entities.SaveChanges();
        }
    }

    public IList<twit> GetAllTwits()
    {
        IList<twit> twits = null;
            twits = Entities.twit.OrderBy(t => t.datetwit).ToList();
            //foreach (twit entity in twits)
            //{
            //   entity.AspNetUsers = getUserByTwit(entity);
            //}
        
        return twits;
    }

    public twit GetById(string id)
    {
        return Entities.twit.SingleOrDefault(x => x.Id.Equals(id));
    }

    public List<twit> GetByUserName(string name)
    {
        return Entities.twit.Where(x => x.AspNetUsers.UserName.Equals(name)).ToList<twit>();
    }

    public AspNetUsers getUserByTwit(twit entity)
    {
        AspNetUsers user = null;
        String[] qwe = new String[1];
        qwe[0] = entity.user_id;
        user = Entities.AspNetUsers.Find(qwe);
        //try {
        //    user = (from i in Entities.AspNetUsers where i.Id == entity.user_id select i).First<AspNetUsers>();
        //}
        //catch (EntityCommandExecutionException ex)
        //{
        //    IDictionary<object, object> dict = () ex.Data;
        //}
        return user;
    }
}