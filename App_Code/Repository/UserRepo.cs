﻿using Classes;
using Microsoft.AspNet.Identity;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TwitterWebFormGIT;

/// <summary>
/// Сводное описание для TwitRepo
/// </summary>
public class UserRepo : AbstractRepo
{
    
    public UserRepo()
    {
        
    }

    public AspNetUsers getCurrentUser()
    {
        string userId = System.Web.HttpContext.Current.User.Identity.GetUserId();
        return Entities.AspNetUsers.Find(userId);
    }

    public string getCurrentUserId()
    {
        return HttpContext.Current.User.Identity.GetUserId();
    }

}