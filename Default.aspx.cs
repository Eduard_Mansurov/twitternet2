﻿using Classes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : Page
{
    public IList<twit> TwitList { get; set; }
    private TwitRepo twitRepo = new TwitRepo();
    private UserRepo userRepo = new UserRepo();
    public String userId = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        userId = userRepo.getCurrentUserId();
        //DataBind();
        //TwitList = twitRepo.GetAllTwits();

        string sessionUser = "";
        try
        {
            sessionUser = Session["userName"].ToString();
        } catch
        {

        }
        string pageUser = Request.QueryString["name"];

        if (pageUser == null || pageUser.Equals(""))
            pageUser = sessionUser;

        SqlTwitDataSource.SelectParameters.Add("pageUser", pageUser);
        SqlTwitDataSource.SelectCommand = "SELECT t.*, u.[UserName], u.[Id] as user_id FROM [dbo].[twit] t LEFT JOIN [AspNetUsers] u ON t.[user_id] = u.[Id] WHERE u.[UserName]=@pageUser ORDER BY [datetwit] DESC";

        if (sessionUser.Equals("") || !pageUser.Equals(sessionUser))
        {
            NewTwitBox.Visible = false;
        }
    }

    protected void SubmitButton_Click(object sender, EventArgs e)
    {

        string twitId = ((WebControl)sender).Attributes["twitid"];
        string text = twitInput.Text;

        if (text != null && !text.Trim().Equals(""))
        {
            twit newTwit = null;

            if (twitId != null)
            {
                newTwit = twitRepo.GetById(twitId);
                newTwit.twittext = text;
                twitRepo.Update(newTwit);

            }
            else {

                newTwit = new twit();
                newTwit.twittext = twitInput.Text;
                newTwit.user_id = userRepo.getCurrentUserId();
                newTwit.Id = Guid.NewGuid().ToString();
                newTwit.datetwit = DateTime.Now;
                newTwit.access = "open";
                newTwit.is_twit = true;
                // newTwit.AspNetUsers = userRepo.getCurrentUser();
                twitRepo.add(newTwit);
            }

            Response.Redirect("/");
        }
    }



    protected void Delete_TwitClick(object sender, EventArgs e)
    {
        string twitId = ((System.Web.UI.HtmlControls.HtmlControl)sender).Attributes["twitid"];
        twitRepo.Delete(twitId);
        Response.Redirect("/");
        //((System.Web.UI.HtmlControls.HtmlControl)sender).Attributes["buttonid"]
    }

    protected void Edit_TwitClick(object sender, EventArgs e)
    {
        string twitId = ((System.Web.UI.HtmlControls.HtmlControl)sender).Attributes["twitid"];
        twitInput.Text = ((System.Web.UI.HtmlControls.HtmlControl)sender).Attributes["twitText"];
        submitButton.Text = "EDIT";
        submitButton.Attributes.Remove("twitid");
        submitButton.Attributes.Add("twitid", twitId);
        //((System.Web.UI.HtmlControls.HtmlControl)sender).Attributes["buttonid"]
    }
}