﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" EnableEventValidation="false" %>
<%@ Register TagPrefix="mycontrol" TagName="Footer" Src="~/FooterUserControl.ascx" %>


<%@ Import Namespace="Classes" %>
<%@ Import Namespace="Microsoft.Ajax.Utilities" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    
        <div class="container top horizontal-center">
        <asp:Panel runat="server" ID="NewTwitBox" CssClass="shadow-z-2 topArea" style="background: white">
            <div class="form-group">
                <div class="row">
                    <div class="col-lg-10">
                        <div class="form-group">
                            <asp:TextBox ID="twitInput" runat="server" CssClass="form-control floating-label twitInput"></asp:TextBox>
                            <p><asp:RegularExpressionValidator ValidationGroup="newTwitGroup" Display = "Dynamic" ControlToValidate = "twitInput" ID="RegularExpressionValidatorForTwitInput" ValidationExpression = "^[\w\d]([\s\S]){5,175}$" CssClass="text-danger"  runat="server" ErrorMessage="Minimum 6 and Maximum 175 characters required."></asp:RegularExpressionValidator></p>
                            <p><asp:RegularExpressionValidator ValidationGroup="newTwitGroup" Display = "Dynamic" ControlToValidate = "twitInput" ID="SpecialCharacterValidator" ValidationExpression = "^[^@;\<\>]+$" runat="server" CssClass="text-danger"  ErrorMessage="You can't use @, <, >, ;"></asp:RegularExpressionValidator></p>
                            <p><asp:RequiredFieldValidator ValidationGroup="newTwitGroup" runat="server" ControlToValidate="twitInput" CssClass="text-danger" ErrorMessage="Empty field" /></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div id="errorText" class="text-danger col-lg-2"
                        style="margin-left: 68%; margin-top: 1%">
                    </div>
                    <div class="teste">
                        <asp:Button ID="submitButton" ValidationGroup="newTwitGroup" runat="server" CssClass="btn btn-raised btn-primary submitButton" OnClick="SubmitButton_Click" Text="SUBMIT" />
                    </div>
                </div>
            </div>
        </asp:Panel>
        <div class="shadow-z-2 fill" style="background: white">
            <div>
                <div class="teste1" style="padding-top: 7%; padding-bottom: 1%">
                    <%--<% foreach (var entry in TwitList)
                        { %>--%>
                    <asp:Repeater ID="TwitRepeater" runat="server"
                        DataSourceID="SqlTwitDataSource">
                        <ItemTemplate>  

                        <div class='twit' > 
                            <strong><%# Eval("UserName") %></strong>
                            <%# Eval("locationtwit") != null ? Eval("locationtwit") : "" %>
                            <div style="float: right" class="text-muted <%# Eval("Id") %>">
                                <script type="text/javascript">
                                    $(".text-muted.<%# Eval("Id") %>").append(moment('<%# Eval("datetwit") %>', 'DD.MM.YYYY hh:mm:ss').format('MMMM Do YYYY, h:mm:ss a'));
                                </script>
                            </div>
                            <div>
                                <h4>
                                    <p class='twitText <%# Eval("Id") %>'><%# Eval("twittext")%></p>
                                </h4>
                            </div>
                            <div class="row">
                                <asp:Panel runat="server" ID="Qaws">
                                    <div class="icon-preview" style="float: right">
                                        <!--<i class="mdi-action-delete deleteButton"><asp:Button class="mdi-action-delete deleteButton" runat="server" Visible="true"/><span>delete</span></i>-->

                                        <button runat="server" onserverclick="Delete_TwitClick" class="mdi-action-delete deleteButton actionButton" twitid='<%# Eval("Id") %>' visible="True" />
                                        <span>delete</span>
                                    </div>
                                    <div class="icon-preview" style="float: right">
                                        <button runat="server" onserverclick="Edit_TwitClick" class="mdi-editor-mode-edit editButton actionButton" visible="True" twitid='<%# Eval("Id") %>' twittext='<%# Eval("twittext") %>'><span>edit</span></button>
                                    </div>
                                </asp:Panel>
                                <div class="icon-preview" style="float: right">
                                    <button runat="server" class="mdi-content-reply replyButton actionButton" twitid='<%# Eval("Id") %>' visible="True"><span>reply</span></button>
                                </div>
                                <div class="privateAccess icon-preview" style="float: right">
                                    <i class="mdi-social-people privateButton"
                                        data-toggle="modal" data-target='<%# Eval("Id") %>'
                                        twitid='<%# Eval("Id") %>'
                                        <%# Eval("access") != null && !((string) Eval("access")).Equals("private") ? "style='display: none'" : "" %>
                                        style="display: none"><span>Edit access</span></i>
                                </div>
                                <div class="private"
                                    messagetype='<%# Eval("is_twit") != null && (bool)Eval("is_twit") ? "twit": "retwit" %>'
                                    twitid='<%# Eval("Id") %>' style="float: right">
                                    <select class="form-control select selectAccess" placeholder="Кому виден твит?">
                                        <option value="open">Открытый твит</option>
                                        <option value="private">Приватный твит</option>
                                        <option value="close">Закрытый от всех</option>
                                    </select>
                                    <script type="text/javascript">$(".select").dropdown({ "autoinit": ".select" });</script>
                                </div>
                                <div id='access<%# Eval("Id") %>' class="modal fade"
                                    tabindex="-1">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close"
                                                    data-dismiss="modal" aria-hidden="true">
                                                    ×
                                                </button>
                                                <h4 class="modal-title">Dialog</h4>
                                            </div>
                                            <div class="modal-body">

                                                <textarea type="text" name="access"
                                                    class='form-control floating-label newAccess<%# Eval("Id") %>'
                                                    rows="3" placeholder="New access"></textarea>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-primary editAccess"
                                                    twitid='<%# Eval("Id") %>' data-dismiss="modal"
                                                    messagetype='<%# Eval("is_twit") != null && (bool)Eval("is_twit") ? "twit": "retwit" %>'>
                                                    EDIT
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>
        <mycontrol:footer ID="mycontrol2" runat="server" />
    </div>

    

    <asp:SqlDataSource ID="SqlTwitDataSource" runat="server"
        ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        >
    </asp:SqlDataSource>
</asp:Content>
